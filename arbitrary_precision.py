#Import the necessary libraries
import numpy as np
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt
from scipy.sparse import spdiags
from scipy.sparse import issparse
# from sample import SamplePlusMaterial as spm
from run import run
from run import state
import time
from scipy.sparse import diags
from decimal import *
from mpmath import *

# +
#This is the solver class, it requires a samplePlusMaterial Class to access the relevant parameters.
class Solver:
    def __init__(self, workbench):
        self.workbench = workbench
        N = workbench.sample.N
        self.K_sparse = np.zeros([N,N])
        self.K2_sparse = np.zeros([N,N])
        
         
    #This is an implementation of the Bernoulli function that takes an array as the argument and 
    #uses the series expansion when the argument is small.
    
    def B(self, x):
        
        t = mpf(x)
        if(almosteq(t, mpf('0'))):
            return np.double(mpf('1'))
        else:
            y =  t/(mp.exp(t) - mpf('1'))
            return np.double(y)
        
        
#         return x/(mp.exp(x) - mpf('1'))
        
#         if(np.abs(x)< 10**-4):
#             return 1 - ((x)/(2)) + ((x)**2/(12)) - ((x)**4/(720)) + ((x)**6/(30240))
#         else:
#             return (x)/(np.expm1(x))
        
    
            
        
        
    def G(self, x):
        
        return (mpf('1') - mp.exp(-x))
        
#         if(np.abs(x)<10**-4):
#             return x*(1 - x/2 + x**2/6 - x**3/24 + x**4/120)
#         else:
#             return -np.expm1(-x)
        
    def H(self, x):
        
        return (mpf('1') - mp.exp(x))
        
        
#         if(np.abs(x)<10**-4):
#             return -x*(1 + x/2 + x**2/6 + x**3/24)
#         else:
#             return -np.expm1(x)

        
    
    
    #This function plots the potentials and concentrations for a given state
    def plot_potentials_and_concentrations(self, state):
        
        psi = state.psi
        phi_p = state.phi_p
        phi_n = state.phi_n
        p = state.p
        n = state.n
        E_c = self.workbench.material.E_c
        E_v = self.workbench.material.E_v
        
        
        plt.plot(-phi_p ,"--g", label=r'$\varphi_p $')
        plt.plot(-phi_n ,"b--", label =r"$\varphi_n $")
        plt.plot( E_c - psi,"k-", label = r"$E_c - \psi $")
        plt.plot( E_v -psi,"r-", label = r"$E_v - \psi$")

        plt.ylabel("Potential")
        plt.xlabel("Node number")
        plt.legend(loc='upper right')
        plt.show()

        plt.title(r"Hole potential" r" $ \varphi_p $",fontsize=16)
        plt.plot(phi_p,"-g")
        plt.ylabel("Potential")
        plt.xlabel("Node number")
        plt.show()

        plt.title(r"Electron potential" r" $ \varphi_n $",fontsize=16)
        plt.plot(phi_n,"b-")
        plt.ylabel("Potential")
        plt.xlabel("Node number")
        plt.show()

        plt.title(r" Hole Concentration" r" $ p(x) $",fontsize=16)
        plt.plot(p,"-g")
        plt.ylabel("Concentration")
        plt.xlabel("Node number")
        plt.show()

        plt.title(r" Electron Concentration" r" $ n(x) $",fontsize=16)
        plt.plot(n,"-b")
        plt.ylabel("Concentration")
        plt.xlabel("Node number")
        plt.show()
    
    
    ## This function calculates the current, but with a modified expression that works on  h = psi[k] - psi[k-1], therefore
    ## incorporating "better" the fact that if the potential differences is zero the currents must be zero.

    #Calculate the electron current
    
    
    
    def calc_current_n(self, psi, phi_n, n):
        sampleMat = self.workbench
        
        N = sampleMat.sample.N
        U_T = mpf(sampleMat.sample.U_T)
        
        j_n = matrix(N,1)
        
        for j in range(N-1):
            deltaPsi = (psi[j+1] - psi[j])/U_T
            deltaPhi_n = (phi_n[j+1] - phi_n[j])/U_T
            
            j_n[j] = -self.B(-deltaPsi)*n[j]*self.G(deltaPhi_n)
            
            
        return j_n
    
    def calc_current_p(self, psi, phi_p, p):
        sampleMat = self.workbench
        
        N = sampleMat.sample.N
        U_T = mpf(sampleMat.sample.U_T)
        
        
        j_p = matrix(N,1)
        
        for j in range(N-1):
            deltaPsi = (psi[j+1] - psi[j])/U_T
            deltaPhi_p = (phi_p[j+1] - phi_p[j])/U_T
            j_p[j] = p[j]*self.B(deltaPsi)*self.H(deltaPhi_p)
             
            
        return j_p
    
    
    def calc_gradF3(self, psi, phi_p, p):
        sampleMat = self.workbench
        
        N = sampleMat.sample.N
        U_T = mpf(sampleMat.sample.U_T)
        
        
        gradF3 = matrix(N,N)
        
        
        for j in range(1,N-1):
            
            deltaPsi = (psi[j+1] - psi[j])/U_T

            gradF3[j,j] = self.B(deltaPsi)
            
            deltaPsi = (psi[j] - psi[j-1])/U_T
            deltaPhi_p = (phi_p[j] - phi_p[j-1])/U_T
            
            gradF3[j,j] += self.B(-deltaPsi)
            
            gradF3[j,j] *= p[j-1]*mp.exp(deltaPhi_p - deltaPsi)/U_T
            
            
        for j in range(1,N-2):
            
            deltaPsi = (psi[j+1] - psi[j])/U_T
            deltaPhi_p = (phi_p[j+1] - phi_p[j])/U_T
            
            gradF3[j,j+1] = -p[j]*self.B(deltaPsi)*mp.exp(deltaPhi_p)/U_T
            
            
        for j in range(2, N-1):
            
            deltaPsi = (psi[j] - psi[j-1])/U_T
            deltaPhi_p = (phi_p[j] - phi_p[j-1])/U_T
            
            
            gradF3[j,j-1] = -p[j-1]*self.B(deltaPsi)/U_T

            
        
        return gradF3
        
        
    
    
    def calc_gradF2(self, psi, phi_n, n):
                
        sampleMat = self.workbench
        
        N = sampleMat.sample.N
        U_T = mpf(sampleMat.sample.U_T)
        
        gradF2 = matrix(N,N)
        
        for j in range(1,N-1):
            deltaPsi = (psi[j+1] - psi[j])/U_T
            
            gradF2[j,j] = self.B(-deltaPsi)
            
            deltaPsi = (psi[j] - psi[j-1])/U_T
            deltaPhi_n = (phi_n[j] - phi_n[j-1])/U_T


            gradF2[j,j] += self.B(deltaPsi)
            
            gradF2[j,j] *= n[j-1]*mp.exp(deltaPsi - deltaPhi_n)/U_T
            
            
        for j in range(1,N-2):
            deltaPsi = (psi[j+1] - psi[j])/U_T
            deltaPhi_n = (phi_n[j+1] - phi_n[j])/U_T
            
            gradF2[j,j+1] = -n[j]*self.B(-deltaPsi)*mp.exp(-deltaPhi_n)/U_T
            
            
        for j in range(2,N-1):
            deltaPsi = (psi[j] - psi[j-1])/U_T
            deltaPhi_n = (phi_n[j] - phi_n[j-1])/U_T
            
            gradF2[j,j-1] = -n[j-1]*self.B(-deltaPsi)/U_T


        return gradF2
        
    
    
    

    
    def calc_built_in_potential(self):
        
        mp.dps = 40
        mp.pretty = False
        
        sampleMat = self.workbench
        
        N = sampleMat.sample.N
        C = matrix(sampleMat.sample.C)
        U_T = mpf(sampleMat.sample.U_T)
        E_c = mpf(sampleMat.material.E_c)
        E_v = mpf(sampleMat.material.E_v)
        kT = mpf(sampleMat.sample.kT)
        qEpsilon = mpf(sampleMat.qEpsilon)
        N_v = mpf(sampleMat.material.N_v)
        N_c = mpf(sampleMat.material.N_c)
        
        #This is the matrix representation of the derivative in the poisson equation for the Newton Raphson solver
        K = np.zeros([N,N])
        psi_0 = matrix(self.workbench.psi_0)
        
        h = matrix(np.ones(N)) ## The separation between successive nodes h[k] = x_(k+1) - x_k. This represents the actual distance.


        for i in range(1,N-1):
            K[i][i] = -1/h[i] -1/h[i+1]
            K[i][i+1] = 1/h[i+1]
            K[i][i-1] = 1/h[i-1]

        K[0][0] =-1
        K[N-1][N-1] = -1
        
        K_sparse  = matrix(K)


        F = np.zeros(N)
        G = matrix(N,1)
        gradG = matrix(N,N)
        n = matrix(N,1)
        p = matrix(N,1)
        phi_p = matrix(N,1)
        phi_n = matrix(N,1)
        j_p = matrix(N,1)
        j_n = matrix(N,1)
        
        psi = psi_0
        
        for i in range(20):
            print(i)
            for j in range(N):
                p[j] = N_v*mp.exp((E_v-psi[j])/kT)
                n[j] = N_c*mp.exp((psi[j]-E_c)/kT)

            for j in range(1,N-1):
                G[j] = qEpsilon*(p[j] - n[j] + C[j])

            G[0] = psi_0[0]
            G[N-1] = psi_0[N-1]

            for j in range(N):
                gradG[j,j] = -qEpsilon*(p[j] +n[j])/U_T


            KgradG = K_sparse + gradG
            b = -K_sparse*psi - G

            deltaPsi = mp.lu_solve(KgradG, b)

            psi = psi + deltaPsi
            
         
        for j in range(N):
            phi_p[j] = mpf('0')
            phi_n[j] = mpf('0')
            j_p[j] = mpf('0')
            j_n[j] = mpf('0')
        
        psi_eq = psi
        phi_p_eq = phi_p
        phi_n_eq = phi_n
        
        built_in_state = state(psi, phi_p, phi_n, p, n, j_p, j_n, 0)
        
        self.plot_potentials_and_concentrations(built_in_state)
        
        return built_in_state
        
            
            
        
        

        

        
    
    
    def arbitrary_precision_drift_diffusion_solver(self, initialState, stepSizeVoltage, numberOfDataPoints, convergence, iterations, displayOutput = 0):
        
        mp.dps = 80
        mp.pretty = False
        
#         iterations = 100
        
        aug_switch = 0.0
        spont_switch = 0.0
        srh_switch = 0.0
        
        
        samplePlusMat = self.workbench
        N_v = mpf(samplePlusMat.material.N_v)
        E_v = mpf(samplePlusMat.material.E_v)
        N_c = mpf(samplePlusMat.material.N_c)
        E_c = mpf(samplePlusMat.material.E_c)
        C = matrix(samplePlusMat.sample.C)
        kT = mpf(samplePlusMat.sample.kT)
        U_T = mpf(samplePlusMat.sample.U_T)
        qEpsilon = mpf(samplePlusMat.qEpsilon)
        N = samplePlusMat.sample.N
        
#         tau_p = samplePlusMat.material.tau_p
#         tau_n = samplePlusMat.material.tau_n
#         n_t = samplePlusMat.n_t
#         p_t = samplePlusMat.p_t
#         r_srh_con = srh_switch*samplePlusMat.r_srh_con
#         r_srh_cop = srh_switch*samplePlusMat.r_srh_cop
#         r_sp_n = spont_switch*samplePlusMat.r_sp_n
#         r_sp_p = spont_switch*samplePlusMat.r_sp_p
        
        j_con = mpf(samplePlusMat.j_con)
        j_cop = mpf(samplePlusMat.j_cop)

#         r_aug_n = aug_switch*samplePlusMat.r_aug_n
#         r_aug_p = aug_switch*samplePlusMat.r_aug_p
        
        N_I = matrix(samplePlusMat.N_I)
        V = mpf('0.1')
        U2 = mpf(stepSizeVoltage*numberOfDataPoints)
        U1 = mpf('0')
        countVoltage = 1
        
        j_n_mean = np.zeros(numberOfDataPoints)
        j_p_mean = np.zeros(numberOfDataPoints)
        j_n_std = np.zeros(numberOfDataPoints)
        j_p_std = np.zeros(numberOfDataPoints)
        run1 = run(numberOfDataPoints, samplePlusMat)

#         r_sp_n = r_sp_p = r_srh_con = r_srh_cop = r_aug_n = r_aug_p = 0
#         iterations = 4000

        #These arrays store the error at the end of each iteration
        errorPlot = np.zeros(iterations)
        errorPlot_p = np.zeros(iterations)
        errorPlot_n = np.zeros(iterations)

        rate_p = 1.0
        rate_n = 1.0
        ratePsi = 1.0

        h = matrix(np.ones(N))
        
        
        F1 = matrix(N,1)    
        gradF1 = matrix(N,N)
        gradF1_diag = matrix(N-2,N-2)
        F1_shifted = matrix(N-2,1)
        psi_shifted = matrix(N-2,1)
        

        F2 = matrix(N,1)
        F2_shifted = matrix(N-2,1)
        gradF2 = matrix(N,N)

#         K2 = np.zeros([N,N])
#         K2[0][0] = K2[N-1][N-1] = -1
#         K2_sparse = csc_matrix(K2)
#         self.K2_sparse = K2_sparse
#         K_sparse = self.K_sparse

        K = np.zeros([N,N])
        for i in range(1,N-1):
            K[i][i] = -1/h[i] -1/h[i+1]
            K[i][i+1] = 1/h[i+1]
            K[i][i-1] = 1/h[i-1]

        K[0][0] =-1
        K[N-1][N-1] = -1
        
        K_sparse  = matrix(K)


        F3 = matrix(N,1)
        F3_shifted = matrix(N-2,1)

        gradF3 = matrix(N,N)


        bias_V = np.zeros(numberOfDataPoints)
        bias_j = np.zeros(numberOfDataPoints)


#         B1 = np.zeros(N)
#         B2 = np.zeros(N)


#         diags = np.array([0, 1, -1])
        gradF2_diag = matrix(N-2,N-2)
        gradF3_diag = matrix(N-2,N-2)
        K_diag = matrix(N-2,N-2)

        
#         self.plot_potentials_and_concentrations(initialState)
        psi = initialState.psi
        phi_p = matrix(np.zeros(N))
        phi_n = matrix(np.zeros(N))
        psi_0 = matrix(self.workbench.psi_0)
        
        p = matrix(N,1)
        n = matrix(N,1)
        
        print(V)
        print("StepSize", stepSizeVoltage)
        start = time.time()
        print("psi_init", psi)
        print("MaxIter",iterations)
        
        while(V <= U2):
#             V = 10
            
            rate_p = 1.0
            ratePsi = 1.0
            rate_n = 1.0
                        
            for i in range(1,N):
                psi[i] += float(i)*(stepSizeVoltage/float(N-1))
                phi_p[i] += float(i)*(stepSizeVoltage/float(N-1))
                phi_n[i] +=  float(i)*(stepSizeVoltage/float(N-1))
                

            psi[0] = psi_0[0]+ mpf(U1)
            psi[N-1] = psi_0[N-1]+ mpf(V)

            phi_p[0] = mpf(U1)
            phi_p[N-1] = mpf(V)
            phi_n[0] = mpf(U1)
            phi_n[N-1] = mpf(V)
            
            plt.plot(psi)
            plt.plot(psi_0)
            plt.ylim(0,1.0)
            plt.show()
            for i in range(iterations):
                
                print(i)
                
                for j in range(N):
                    p[j] = N_v*mp.exp((E_v - psi[j] + phi_p[j])/kT)
                    n[j] = N_c*mp.exp((psi[j] - phi_n[j] - E_c)/kT)
                    F1[j] =  qEpsilon*(p[j] - n[j] + C[j])
                    
                
            
            
#                 F1 = qEpsilon*(p - n + C)
#                 F1[0] = psi_0[0] + mpf(U1)
#                 F1[N-1] = psi_0[N-1] + mpf(V)
                
                
                for j in range(N-2):
                    F1_shifted[j] = F1[j+1]
                    psi_shifted[j] = psi[j+1]
    
                
                for j in range(N):
                    gradF1[j,j] = -qEpsilon*(p[j] + n[j])/U_T     

#                 data[0] =  data[0]*0.5
#                 data[N-1] = data[N-1]*0.5

                for j in range(N-2):
                    gradF1_diag[j,j] = gradF1[j+1,j+1]
                    K_diag[j,j] = K_sparse[j+1,j+1]
                    
                for j in range(N-3):
                    gradF1_diag[j,j+1] = gradF1[j+1,j+2]
                    K_diag[j,j+1] = K_sparse[j,j+1]
                for j in range(1,N-2):
                    gradF1_diag[j,j-1] = gradF1[j+1,j]
                    K_diag[j,j-1] = K_sparse[j+1,j]
    
    
    
#                 deltaPsi = mp.lu_solve(K_diag + gradF1_diag, -K_diag*psi_shifted - F1_shifted)
                deltaPsi = mp.lu_solve(K_sparse + gradF1, -K_sparse*psi - F1)

#                 deltaPsi = U_T*np.log(1 + np.abs(deltaPsi)/U_T)*np.sign(deltaPsi)

                for j in range(N):
                    psi[j] = psi[j] + deltaPsi[j]
#                     psi[j] = psi[j] + U_T*mp.log(1 + mp.fabs(deltaPsi[j])/U_T)*mp.sign(deltaPsi[j])
        
#                 for j in range(1,N-1):
# #                     psi[j] = psi[j] + deltaPsi[j-1]
#                     psi[j] = psi[j] + U_T*mp.log(1 + mp.fabs(deltaPsi[j-1])/U_T)*mp.sign(deltaPsi[j-1])
            
    
                errorPlot[i] = mp.log10(mp.norm(deltaPsi, p=inf))

                for j in range(N):
                    p[j] = N_v*mp.exp((E_v - psi[j] + phi_p[j])/kT)
                    n[j] = N_c*mp.exp((psi[j] - phi_n[j] - E_c)/kT)



#                 #ELECTRON CURRENTS
#                 #Calculate the updated charge concentrations and densities

                j_n = self.calc_current_n(psi, phi_n, n)
                
                
                for j in range(1,N-1):
                    F2[j] = j_n[j] - j_n[j-1]
                    
                    
                
                    
                #F2 shifted by one because the end points are redundant
                for j in range(N-2):
                    F2_shifted[j] = F2[j+1]
                    
                    
                gradF2 = self.calc_gradF2(psi, phi_n, n)
                
                gradF2_np = np.zeros([N,N])
                
                for j in range(N-2):
                    gradF2_diag[j,j] = gradF2[j+1,j+1]
                    gradF2_np[j][j] = gradF2[j+1,j+1]
                    
                for j in range(N-3):
                    gradF2_diag[j,j+1] = gradF2[j+1,j+2]
                    gradF2_np[j][j+1] = gradF2[j+1,j+2]
                for j in range(1,N-2):
                    gradF2_diag[j,j-1] = gradF2[j+1,j]
                    gradF2_np[j][j-1] = gradF2[j+1,j]
            
                
                
                
                
#                 plt.imshow(gradF2_np)
#                 plt.colorbar()
#                 plt.show()
                
#                 plt.plot(F2_shifted)
#                 plt.show()
                
#                 import pdb;pdb.set_trace()
                
                deltaPhi_n = mp.lu_solve(gradF2_diag, -F2_shifted)
        
                
#                 deltaPhi_n = U_T*np.log(1 + np.abs(deltaPhi_n)/U_T)*np.sign(deltaPhi_n)
#                 phi_n[1:N-1] = phi_n[1:N-1] + deltaPhi_n

                errorPlot_n[i] = mp.log10(mp.norm(deltaPhi_n, p=inf))
                
        
                for j in range(1,N-1):
                    phi_n[j] = phi_n[j] + U_T*mp.log(1 + mp.fabs(deltaPhi_n[j-1])/U_T)*mp.sign(deltaPhi_n[j-1])
#                     phi_n[j] = phi_n[j] + deltaPhi_n[j-1]
                
                
                
                #HOLE CURRENTS
                #Calculate the updated charge concentrations and densities

                
                
                j_p = self.calc_current_p(psi, phi_p, p)
                
            
                for j in range(1,N-1):
                    F3[j] = j_p[j] - j_p[j-1]
                    
                #F3 shifted by one because the end points are redundant
                for j in range(N-2):
                    F3_shifted[j] = F3[j+1]
                    
                    
                

                gradF3 = self.calc_gradF3(psi, phi_p, p)
                

                
                for j in range(N-2):
                    gradF3_diag[j,j] = gradF3[j+1,j+1]
                    
                for j in range(N-3):
                    gradF3_diag[j,j+1] = gradF3[j+1,j+2]
                    
                for j in range(1,N-2):
                    gradF3_diag[j,j-1] = gradF3[j+1,j]
                    
                                   

                deltaPhi_p = mp.lu_solve(gradF3_diag, -F3_shifted)
#                 deltaPhi_p = U_T*np.log(1 + np.abs(deltaPhi_p)/U_T)*np.sign(deltaPhi_p)
#                 phi_p[1:N-1] = phi_p[1:N-1] + deltaPhi_p

                
                for j in range(1,N-1):
                    phi_p[j] = phi_p[j] + U_T*mp.log(1 + mp.fabs(deltaPhi_p[j-1])/U_T)*mp.sign(deltaPhi_p[j-1])
#                     phi_p[j] = phi_p[j] + deltaPhi_p[j-1]
                    
                errorPlot_p[i] = mp.log10(mp.norm(deltaPhi_p, p=inf))


                #Check if the solution has converged
                if(errorPlot_n[i] < convergence and errorPlot[i] < convergence and errorPlot_p[i] < convergence):
                    print("Iterations", i)
                    break

            #Gradually increase the bias across the junction and store I-V values
            print(V)
        
#             print(j_n)
#             print(j_p)
            
            
            j_n_np = np.array(j_n.tolist(), dtype=np.double)
            j_p_np = np.array(j_p.tolist(), dtype=np.double)
            psi_np = np.array(psi.tolist(), dtype=np.double)
            phi_p_np = np.array(phi_p.tolist(), dtype=np.double)
            phi_n_np = np.array(phi_n.tolist(), dtype=np.double)
            p_np = np.array(p.tolist(), dtype=np.double)
            n_np = np.array(n.tolist(), dtype=np.double)

            


            
            tempState = state(psi_np, phi_p_np, phi_n_np, p_np, n_np, j_p_np, j_n_np, V)
            run1.add(tempState, N)

            
#             print("J_P", j_p_np, type(j_p_np))
            
#             print(np.mean(j_p_np[:N-1]))
            
            if(displayOutput == 1):
                self.plot_potentials_and_concentrations(tempState)
                
                bias_V[countVoltage-1] = V
                bias_j[countVoltage-1] = np.abs(np.mean(j_n_np[:N-1]))
                j_n_mean[countVoltage-1] = np.mean(j_n_np[:N-1])
                j_n_std[countVoltage -1] = np.std(j_n_np[:N-1])                           
                print("j_n_mean:",np.double(j_con*j_n_mean[countVoltage-1]))
                print("j_n_std:", j_n_std[countVoltage -1])
    #             print(j_n*j_con)                                
                print("Error in phi_n",errorPlot_n[i])
                j_p_mean[countVoltage-1] = np.mean(j_p_np[:N-1])
                j_p_std[countVoltage-1] = np.std(j_p_np[:N-1])                           
                print("j_p_mean:",np.double(j_cop*j_p_mean[countVoltage-1]))
                print("j_p_std:", j_p_std[countVoltage -1])
    #             print(j_p*j_cop)
                print("Error in phi_p",errorPlot_p[i])
                print("Error in psi", errorPlot[i])
            
                
                
                const1 = np.double(j_cop*j_p_mean[countVoltage-1])
                print(const1)
                plt.title(r" j_p",fontsize=16)
                plt.plot((j_cop*((j_p_np[:N-1]))),"-g", label='Hole Current')
                plt.ylim(const1 - const1*(10**-4), const1 + const1*(10**-4))

                plt.ylabel("j")
                plt.xlabel("Node number")
                plt.show()

                const2 = np.double(j_con*j_n_mean[countVoltage-1])
                
                plt.title(r" j_n",fontsize=16)
                plt.plot((j_con*((j_n_np[:N-1]))),"-b", label='Electron Current')
                plt.ylim(const2 - const2*(10**-8), const2 + const2*(10**-8))

                plt.ylabel("j")
                plt.xlabel("Node number")
                plt.show()


                plt.plot(errorPlot,"-r")
                plt.plot(errorPlot_p,"-b")
                plt.plot(errorPlot_n,"-g")

                plt.ylabel("Error")
                plt.xlabel("tries")
                plt.show()

                plt.title(r" Error in Electron potential" r" $ \Phi_n $",fontsize=16)
                plt.plot(errorPlot_n,"-g")
                plt.ylabel("Error")
                plt.xlabel("tries")
                plt.show()

                plt.title(r" Error in Hole potential" r" $ \Phi_p $",fontsize=16)
                plt.plot(errorPlot_p,"-b")
                plt.ylabel("Error")
                plt.xlabel("tries")
                plt.show()

                plt.title(r" Error in potential" r" $ \Psi $",fontsize=16)
                plt.plot(errorPlot,"-r")
                plt.ylabel("Error")
                plt.xlabel("tries")
                plt.show() 
                
                countVoltage += 1

                
#                 import pdb; pdb.set_trace()
            
    
    
            if(i>iterations-2):
                print("CONVERGENCE LIMIT NOT REACHED")
                print(errorPlot_p[i],errorPlot_n[i], errorPlot[i] )

        
            V += stepSizeVoltage
            
        end = time.time()
        print(end-start)
        print("Done")
        return run1
            

                
# -








