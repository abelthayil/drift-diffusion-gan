# +
# from __future__ import print_function
#Importing the necessary Python libraries

import numpy as np
import sample
from sample import material as mat
from sample import SamplePlusMaterial as spm
import solver2
import arbitrary_precision
import material_list

from run import run
from run import state
from dp import run_file

from ipywidgets import interact, interactive, fixed, interact_manual, FloatSlider
import ipywidgets as widgets

import time
import pickle 
# +
#The interface class contains the samplePlusMaterial class and the solver class.
#It controls the communication between the solver routines and the sample details
#specified in the samplePlusMaterial class. In addition, it controls the way the
#simulation runs are stored.


class interface:
    #Once initialized it brings up the set paramters function, where the doping
    #profile, length, discretization length, etc are set. 
    def __init__(self):
        self.setParams()
    
    #The set parameters function 
    def setParams(self):
        
        #Reads the simulation parameters from a "run-file" which is defined using "launcher.ipynb"
        with open("run-commands/run-file", 'rb') as f:
            data = pickle.load(f)

        
        sampleTest = sample.Sample(data.N, data.L, data.T, data.C) #Set the sample parameters
        materialTest = data.mat #Set the material
        self.workBench = spm(sampleTest, materialTest) #combine the sample and material
        
        self.workBench.calc_initial_guess() #Calculate initial guess via charge conservation

#         self.solver = explicit_solver.Solver(self.workBench)
#         self.solver = solver2.Solver(self.workBench)
        self.solver = arbitrary_precision.Solver(self.workBench)
#         self.solver = hyperbolic_solver.Solver(self.workBench) #Call a specific solver class
#         self.solver = solver.Solver(self.workBench)
        init = self.solver.calc_built_in_potential() #call the solver's built_in_potential class
    
    
    
    
    
#         #Store the run returned in a file name specified by the run-file
#         run_returned = self.solver.drift_diffusion_solver(init, data.initial_pot, data.numberOfDataPoints, data.convergence, data.maxIter, 1)
        run_returned = self.solver.arbitrary_precision_drift_diffusion_solver(init, data.stepSize, data.numberOfDataPoints, data.convergence, data.maxIter, 1)

#         run_returned = self.solver.drift_diffusion_solver(init, data.stepSize, data.numberOfDataPoints, data.convergence, data.maxIter, 1)
        timestr = time.strftime("%Y%m%d-%H%M")

        data.savePath = data.savePath+"-"+timestr+".pickle"
        
        
        with open(data.savePath, 'wb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
            pickle.dump(run_returned, f, pickle.HIGHEST_PROTOCOL)

test = interface()
# +
# class interface:
#     #Once initialized it brings up the set paramters function, where the doping
#     #profile, length, discretization length, etc are set. 
#     def __init__(self):
#         self.setParams()
    
#     #The set parameters function 
#     def setParams(self):
        
#         #Reads the simulation parameters from a "run-file" which is defined using "launcher.ipynb"
#         with open("run-commands/run-file", 'rb') as f:
#             data = pickle.load(f)

        
#         sampleTest = sample.Sample(data.N, data.L, data.T, data.C) #Set the sample parameters
#         materialTest = data.mat #Set the material
#         self.workBench = spm.SamplePlusMaterial(sampleTest, materialTest) #combine the sample and material
        
#         self.workBench.calc_initial_guess() #Calculate initial guess via charge conservation

#         self.solver = explicit_solver.Solver(self.workBench)

# #         self.solver = arbitrary_precision.Solver(self.workBench)
# #         self.solver = hyperbolic_solver.Solver(self.workBench) #Call a specific solver class
# #         self.solver = solver.Solver(self.workBench)
#         init = self.solver.calc_built_in_potential() #call the solver's built_in_potential class
    
    
    
    
    
# #         #Store the run returned in a file name specified by the run-file
# #         run_returned = self.solver.drift_diffusion_solver(init, data.initial_pot, data.numberOfDataPoints, data.convergence, data.maxIter, 1)
#         run_returned = self.solver.drift_diffusion_solver(init, data.initial_pot, data.numberOfDataPoints, data.convergence, data.maxIter, 1)

# #         with open(data.savePath, 'wb') as f:
# #         # Pickle the 'data' dictionary using the highest protocol available.
# #             pickle.dump(run_returned, f, pickle.HIGHEST_PROTOCOL)

# test = interface()
# -




