# +
#Import the necessary libraries
import numpy as np
import scipy
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt
from scipy.sparse import spdiags
from scipy.sparse import issparse
from sample import SamplePlusMaterial as spm
from run import run
from run import state
import time
from scipy.sparse import diags
from mpmath import *


#This is the solver class, it requires a samplePlusMaterial Class to access the relevant parameters.
class Solver:
    def __init__(self, workbench):
        self.workbench = workbench
        N = workbench.sample.N
        self.K_sparse = np.zeros([N,N])
        self.K2_sparse = np.zeros([N,N])
        mp.dps = 100
        mp.pretty = False

        
        
    #This is an implementation of the Bernoulli function that takes an array as the argument and 
    #uses the series expansion when the argument is small.
    
    def B(self, x):
        
        t = mpf(x)
        if(almosteq(t, mpf('0'))):
            return np.double(mpf('1'))
        else:
            y =  t/(mp.exp(t) - mpf('1'))
            return np.double(y)
        
    
    def G(self, x):
        t = mpf(x)
        y = (mpf('1') - mp.exp(-x))
        return np.double(y)
    
    def H(self, x):
        t = mpf(x)
        y = (mpf('1') - mp.exp(x))
        return np.double(y)
    
    def N(self, x):
        t = mpf(x)
        return np.double(mp.exp(t))
    def P(self, x):
        t = mpf(x)
        return np.double(mp.exp(t))
    
    def exponential(self, x):
        t = mpf(x)
        return np.double(mp.exp(t))
        
        
        
    def calc_n(self, psi, phi_n):
        sampleMat = self.workbench
        N = sampleMat.sample.N
        E_c = (sampleMat.material.E_c)
        kT = (sampleMat.sample.kT)
        n = np.zeros(N)
        N_c = (sampleMat.material.N_c)
        for j in range(N):
            n[j] = N_c*self.N((psi[j] - phi_n[j] - E_c)/kT)
        
        return n
    
    def calc_p(self, psi, phi_p):
        sampleMat = self.workbench
        N = sampleMat.sample.N
        E_v = (sampleMat.material.E_v)
        kT = (sampleMat.sample.kT)
        N_v = (sampleMat.material.N_v)

        p = np.zeros(N)
        for j in range(N):
            p[j] = N_v*self.P((E_v - psi[j] + phi_p[j])/kT)
        
        return p

        
    
    def calc_current_n(self, psi, phi_n, n):
        sampleMat = self.workbench
        
        N = sampleMat.sample.N
        U_T = (sampleMat.sample.U_T)
        
        j_n = np.zeros(N)
        
        for j in range(N-1):
            deltaPsi = (psi[j+1] - psi[j])/U_T
            deltaPhi_n = (phi_n[j+1] - phi_n[j])/U_T
            
            j_n[j] = -self.B(-deltaPsi)*n[j]*self.G(deltaPhi_n)
            
            
        return j_n
    
    def calc_current_p(self, psi, phi_p, p):
        sampleMat = self.workbench
        
        N = sampleMat.sample.N
        U_T = (sampleMat.sample.U_T)
        
        
        j_p = np.zeros(N)
        
        for j in range(N-1):
            deltaPsi = (psi[j+1] - psi[j])/U_T
            deltaPhi_p = (phi_p[j+1] - phi_p[j])/U_T
            j_p[j] = p[j]*self.B(deltaPsi)*self.H(deltaPhi_p)
             
            
        return j_p
    
    def calc_gradF3(self, psi, phi_p, p):
        sampleMat = self.workbench
        
        N = sampleMat.sample.N
        U_T = (sampleMat.sample.U_T)
        
        
        gradF3 = np.zeros([N,N])
        
        
        for j in range(1,N-1):
            
            deltaPsi = (psi[j+1] - psi[j])/U_T

            gradF3[j][j] = self.B(deltaPsi)
            
            deltaPsi = (psi[j] - psi[j-1])/U_T
            deltaPhi_p = (phi_p[j] - phi_p[j-1])/U_T
            
            gradF3[j][j] += self.B(-deltaPsi)
            
            gradF3[j][j] *= p[j-1]*self.exponential(deltaPhi_p - deltaPsi)/U_T
            
            
        for j in range(1,N-2):
            
            deltaPsi = (psi[j+1] - psi[j])/U_T
            deltaPhi_p = (phi_p[j+1] - phi_p[j])/U_T
            
            gradF3[j][j+1] = -p[j]*self.B(deltaPsi)*self.exponential(deltaPhi_p)/U_T
            
            
        for j in range(2, N-1):
            
            deltaPsi = (psi[j] - psi[j-1])/U_T
            deltaPhi_p = (phi_p[j] - phi_p[j-1])/U_T
            
            
            gradF3[j][j-1] = -p[j-1]*self.B(deltaPsi)/U_T

            
        
        return gradF3
    
    
    def calc_gradF2(self, psi, phi_n, n):
                
        sampleMat = self.workbench
        
        N = sampleMat.sample.N
        U_T = (sampleMat.sample.U_T)
        
        gradF2 = np.zeros([N,N])
        
        for j in range(1,N-1):
            deltaPsi = (psi[j+1] - psi[j])/U_T
            
            gradF2[j][j] = self.B(-deltaPsi)
            
            deltaPsi = (psi[j] - psi[j-1])/U_T
            deltaPhi_n = (phi_n[j] - phi_n[j-1])/U_T


            gradF2[j][j] += self.B(deltaPsi)
            
            gradF2[j][j] *= n[j-1]*self.exponential(deltaPsi - deltaPhi_n)/U_T
            
            
        for j in range(1,N-2):
            deltaPsi = (psi[j+1] - psi[j])/U_T
            deltaPhi_n = (phi_n[j+1] - phi_n[j])/U_T
            
            gradF2[j][j+1] = -n[j]*self.B(-deltaPsi)*self.exponential(-deltaPhi_n)/U_T
            
            
        for j in range(2,N-1):
            deltaPsi = (psi[j] - psi[j-1])/U_T
            deltaPhi_n = (phi_n[j] - phi_n[j-1])/U_T
            
            gradF2[j][j-1] = -n[j-1]*self.B(-deltaPsi)/U_T


        return gradF2
    
    
    #This function plots the potentials and concentrations for a given state
    def plot_potentials_and_concentrations(self, state):
        
        psi = state.psi
        phi_p = state.phi_p
        phi_n = state.phi_n
        p = state.p
        n = state.n
        E_c = self.workbench.material.E_c
        E_v = self.workbench.material.E_v
        
        
        plt.plot(-phi_p ,"--g", label=r'$\varphi_p $')
        plt.plot(-phi_n ,"b--", label =r"$\varphi_n $")
        plt.plot( E_c - psi,"k-", label = r"$E_c - \psi $")
        plt.plot( E_v -psi,"r-", label = r"$E_v - \psi$")

        plt.ylabel("Potential")
        plt.xlabel("Node number")
        plt.legend(loc='upper right')
        plt.show()

        plt.title(r"Hole potential" r" $ \varphi_p $",fontsize=16)
        plt.plot(phi_p,"-g")
        plt.ylabel("Potential")
        plt.xlabel("Node number")
        plt.show()

        plt.title(r"Electron potential" r" $ \varphi_n $",fontsize=16)
        plt.plot(phi_n,"b-")
        plt.ylabel("Potential")
        plt.xlabel("Node number")
        plt.show()

        plt.title(r" Hole Concentration" r" $ p(x) $",fontsize=16)
        plt.plot(p,"-g")
        plt.ylabel("Concentration")
        plt.xlabel("Node number")
        plt.show()

        plt.title(r" Electron Concentration" r" $ n(x) $",fontsize=16)
        plt.plot(n,"-b")
        plt.ylabel("Concentration")
        plt.xlabel("Node number")
        plt.show()
    
    def calc_built_in_potential(self):
        
        
        sampleMat = self.workbench
        
        N = sampleMat.sample.N
        C = np.copy(sampleMat.sample.C)
        U_T = (sampleMat.sample.U_T)
        E_c = (sampleMat.material.E_c)
        E_v = (sampleMat.material.E_v)
        kT = (sampleMat.sample.kT)
        qEpsilon = (sampleMat.qEpsilon)
        N_v = (sampleMat.material.N_v)
        N_c = (sampleMat.material.N_c)
        
        #This is the matrix representation of the derivative in the poisson equation for the Newton Raphson solver
        K = np.zeros([N,N])
        psi_0 = np.copy(self.workbench.psi_0)
        
        h = (np.ones(N)) ## The separation between successive nodes h[k] = x_(k+1) - x_k. This represents the actual distance.


        for i in range(1,N-1):
            K[i][i] = -1/h[i] -1/h[i+1]
            K[i][i+1] = 1/h[i+1]
            K[i][i-1] = 1/h[i-1]

        K[0][0] =-1
        K[N-1][N-1] = -1
        
        K_sparse  = np.copy(K)


        F = np.zeros(N)
        gradF = np.zeros([N,N])
        n = np.zeros(N)
        p = np.zeros(N)
        phi_p = np.zeros(N)
        phi_n = np.zeros(N)
        j_p = np.zeros(N)
        j_n = np.zeros(N)
        
        psi = psi_0
        
        for i in range(20):
#             for j in range(N):
#                 p[j] = N_v*np.exp((E_v-psi[j])/kT)
#                 n[j] = N_c*np.exp((psi[j]-E_c)/kT)

            p = self.calc_p(psi, phi_p)
            n = self.calc_n(psi, phi_n)
            
            for j in range(1,N-1):
                F[j] = qEpsilon*(p[j] - n[j] + C[j])

            F[0] = psi_0[0]
            F[N-1] = psi_0[N-1]

            for j in range(N):
                gradF[j][j] = -qEpsilon*(p[j] +n[j])/U_T


            KgradF = K_sparse + gradF
            b = -np.dot(K_sparse,psi) - F

            deltaPsi = np.linalg.solve(KgradF, b)
            psi = psi + deltaPsi
            
         
        
        
        psi_eq = psi
        phi_p_eq = phi_p
        phi_n_eq = phi_n
        
        built_in_state = state(psi, phi_p, phi_n, p, n, j_p, j_n, 0)
        self.plot_potentials_and_concentrations(built_in_state)
        
        return built_in_state
    
    
    
    def drift_diffusion_solver(self, initialState, stepSizeVoltage, numberOfDataPoints, convergence, iterations, displayOutput = 0):
                
        
#         convergence = -12
        
        aug_switch = 0.0
        spont_switch = 0.0
        srh_switch = 0.0
        
        
        samplePlusMat = self.workbench
        N_v = (samplePlusMat.material.N_v)
        E_v = (samplePlusMat.material.E_v)
        N_c = (samplePlusMat.material.N_c)
        E_c = (samplePlusMat.material.E_c)
        C = np.copy(samplePlusMat.sample.C)
        kT = (samplePlusMat.sample.kT)
        U_T = (samplePlusMat.sample.U_T)
        qEpsilon = (samplePlusMat.qEpsilon)
        N = samplePlusMat.sample.N
        
#         tau_p = samplePlusMat.material.tau_p
#         tau_n = samplePlusMat.material.tau_n
#         n_t = samplePlusMat.n_t
#         p_t = samplePlusMat.p_t
#         r_srh_con = srh_switch*samplePlusMat.r_srh_con
#         r_srh_cop = srh_switch*samplePlusMat.r_srh_cop
#         r_sp_n = spont_switch*samplePlusMat.r_sp_n
#         r_sp_p = spont_switch*samplePlusMat.r_sp_p
        
        j_con = (samplePlusMat.j_con)
        j_cop = (samplePlusMat.j_cop)

#         r_aug_n = aug_switch*samplePlusMat.r_aug_n
#         r_aug_p = aug_switch*samplePlusMat.r_aug_p
        
        N_I = np.copy(samplePlusMat.N_I)
        V = stepSizeVoltage
        U2 = (stepSizeVoltage*numberOfDataPoints)
        U1 = 0
        countVoltage = 1
        
        j_n_mean = np.zeros(numberOfDataPoints)
        j_p_mean = np.zeros(numberOfDataPoints)
        j_n_std = np.zeros(numberOfDataPoints)
        j_p_std = np.zeros(numberOfDataPoints)
        run1 = run(numberOfDataPoints, samplePlusMat)

#         r_sp_n = r_sp_p = r_srh_con = r_srh_cop = r_aug_n = r_aug_p = 0
#         iterations = 4000

        #These arrays store the error at the end of each iteration
        errorPlot = np.zeros(iterations)
        errorPlot_p = np.zeros(iterations)
        errorPlot_n = np.zeros(iterations)

        rate_p = 1.0
        rate_n = 1.0
        ratePsi = 1.0

        h = (np.ones(N))
        
        
        F1 = np.zeros(N)    
        gradF1 = np.zeros([N,N])
        

        F2 = np.zeros(N)
        F2_shifted = np.zeros(N-2)
        gradF2 = np.zeros([N,N])

#         K2 = np.zeros([N,N])
#         K2[0][0] = K2[N-1][N-1] = -1
#         K2_sparse = csc_matrix(K2)
#         self.K2_sparse = K2_sparse
#         K_sparse = self.K_sparse

        K = np.zeros([N,N])
        for i in range(1,N-1):
            K[i][i] = -1/h[i] -1/h[i+1]
            K[i][i+1] = 1/h[i+1]
            K[i][i-1] = 1/h[i-1]

        K[0][0] =-1
        K[N-1][N-1] = -1
        
        K_sparse  = np.copy(K)


        F3 = np.zeros(N)
        F3_shifted = np.zeros(N-2)

        gradF3 = np.zeros([N,N])


        bias_V = np.zeros(numberOfDataPoints)
        bias_j = np.zeros(numberOfDataPoints)


#         B1 = np.zeros(N)
#         B2 = np.zeros(N)


#         diags = np.array([0, 1, -1])
        gradF2_diag = np.zeros([N-2,N-2])
        gradF3_diag = np.zeros([N-2,N-2])

        
#         self.plot_potentials_and_concentrations(initialState)
        psi = initialState.psi
        phi_p = (np.zeros(N))
        phi_n = (np.zeros(N))
        psi_0 = np.copy(self.workbench.psi_0)
        
        p = np.zeros(N)
        n = np.zeros(N)
        
        print(V)
        start = time.time()
        print("psi_init", psi)
    
        while(V <= U2):
#             V = 10
            
            rate_p = 1.0
            ratePsi = 1.0
            rate_n = 1.0
                        
            for i in range(1,N):
                psi[i] += float(i)*(stepSizeVoltage/float(N-1))
                phi_p[i] += float(i)*(stepSizeVoltage/float(N-1))
                phi_n[i] +=  float(i)*(stepSizeVoltage/float(N-1))

            psi[0] = psi_0[0]+ (U1)
            psi[N-1] = psi_0[N-1]+ (V)
            phi_p[0] = (U1)
            phi_p[N-1] = (V)
            phi_n[0] = (U1)
            phi_n[N-1] = (V)
            
            

            for i in range(iterations):
                
                print(i)
                
                p = self.calc_p(psi, phi_p)
                n = self.calc_n(psi, phi_n)
                    
                
            
            
                F1 = qEpsilon*(p - n + C)
                F1[0] = psi_0[0] + U1
                F1[N-1] = psi_0[N-1] + V
                
                
    
                
                for j in range(N):
                    gradF1[j][j] = -qEpsilon*(p[j] + n[j])/U_T     

#                 data[0] =  data[0]*0.5
#                 data[N-1] = data[N-1]*0.5

                deltaPsi = spsolve(csc_matrix(K_sparse + gradF1), -K_sparse.dot(psi) - F1)

                deltaPsi = scipy.linalg.solve(K_sparse + gradF1, -K_sparse.dot(psi) - F1)
                deltaPsi = U_T*np.log(1 + np.abs(deltaPsi)/U_T)*np.sign(deltaPsi)
                psi = psi + deltaPsi
    
                errorPlot[i] = np.log10(np.max(np.abs(deltaPsi)))

                p = self.calc_p(psi, phi_p)
                n = self.calc_n(psi, phi_n)

                    
                    
                    
                    
                #ELECTRON CURRENTS

                j_n = self.calc_current_n(psi, phi_n, n)
                
                
                for j in range(1,N-1):
                    F2[j] = j_n[j] - j_n[j-1]
                    
                #F2 shifted by one because the end points are redundant
                for j in range(N-2):
                    F2_shifted[j] = F2[j+1]
                    
                    
                gradF2 = self.calc_gradF2(psi, phi_n, n)
                
                
                
                for j in range(N-2):
                    gradF2_diag[j][j] = gradF2[j+1][j+1]
                    
                for j in range(N-3):
                    gradF2_diag[j][j+1] = gradF2[j+1][j+2]
                    
                for j in range(1,N-2):
                    gradF2_diag[j][j-1] = gradF2[j+1][j]
                    
            

                deltaPhi_n = spsolve(csc_matrix(gradF2_diag), (-F2_shifted))

#                 deltaPhi_n = scipy.linalg.solve(gradF2_diag, -F2_shifted)
        
                
                deltaPhi_n = U_T*np.log(1 + np.abs(deltaPhi_n)/U_T)*np.sign(deltaPhi_n)
#                 phi_n[1:N-1] = phi_n[1:N-1] + deltaPhi_n

                errorPlot_n[i] = np.log10(np.max(np.abs(deltaPhi_n)))

                for j in range(1,N-1):
                    phi_n[j] = phi_n[j] + deltaPhi_n[j-1]
                
                
#                 plt.imshow(gradF2)
#                 plt.colorbar()
#                 plt.show()
                
#                 print(deltaPhi_n)
#                 print(phi_n)
#                 import pdb;pdb.set_trace()

                
                #HOLE CURRENTS
                #Calculate the updated charge concentrations and densities

                
                
                j_p = self.calc_current_p(psi, phi_p, p)
                
            
                for j in range(1,N-1):
                    F3[j] = j_p[j] - j_p[j-1]
                    
                #F3 shifted by one because the end points are redundant
                for j in range(N-2):
                    F3_shifted[j] = F3[j+1]
                    
                    
                

                gradF3 = self.calc_gradF3(psi, phi_p, p)
                

                
                for j in range(N-2):
                    gradF3_diag[j][j] = gradF3[j+1][j+1]
                    
                for j in range(N-3):
                    gradF3_diag[j][j+1] = gradF3[j+1][j+2]
                    
                for j in range(1,N-2):
                    gradF3_diag[j][j-1] = gradF3[j+1][j]
                    
                                   
                deltaPhi_p = spsolve(csc_matrix(gradF3_diag), -F3_shifted)
#                 deltaPhi_p = scipy.linalg.solve(gradF3_diag, -F3_shifted)
                deltaPhi_p = U_T*np.log(1 + np.abs(deltaPhi_p)/U_T)*np.sign(deltaPhi_p)
#                 phi_p[1:N-1] = phi_p[1:N-1] + deltaPhi_p

                
                for j in range(1,N-1):
                    phi_p[j] = phi_p[j] + deltaPhi_p[j-1]
                    
                errorPlot_p[i] = np.log10(np.max(np.abs(deltaPhi_p)))
                
#                 plt.imshow(gradF3_diag)
#                 plt.colorbar()
#                 plt.show()
                
#                 plt.plot(j_p)
#                 plt.show()
#                 plt.plot(j_n)
#                 plt.show()
                
#                 plt.plot(p)
#                 plt.plot(n)
#                 plt.show()
                
#                 plt.plot(phi_p)
#                 plt.plot(phi_n)
#                 plt.show()
                
#                 print("F3",F3_shifted, "F2",F2_shifted)
#                 print(deltaPhi_p)
#                 print(phi_p)
#                 import pdb;pdb.set_trace()

                #Check if the solution has converged
                if(errorPlot_n[i] < convergence and errorPlot[i] < convergence and errorPlot_p[i] < convergence):
                    print("Iterations", i)
                    break

            #Gradually increase the bias across the junction and store I-V values
            print(V)
            
            p = self.calc_p(psi, phi_p)
            n = self.calc_n(psi, phi_n)


            
            j_n = self.calc_current_n(psi, phi_n, n)
            j_p = self.calc_current_p(psi, phi_p, p)
            
            
            tempState = state(psi, phi_p, phi_n, p, n, j_p, j_n, V)
            run1.add(tempState, N)

            
#             print("J_P", j_p_np, type(j_p_np))
            
#             print(np.mean(j_p_np[:N-1]))
            
            if(displayOutput == 1):
#                 self.plot_potentials_and_concentrations(tempState)
                
                bias_V[countVoltage-1] = V
                bias_j[countVoltage-1] = np.abs(np.mean(j_n[:N-1]))
                j_n_mean[countVoltage-1] = np.mean(j_n[:N-1])
                j_n_std[countVoltage -1] = np.std(j_n[:N-1])                           
                print("j_n_mean:",np.double(j_con*j_n_mean[countVoltage-1]))
                print("j_n_std:", j_n_std[countVoltage -1])
    #             print(j_n*j_con)                                
                print("Error in phi_n",errorPlot_n[i])
                j_p_mean[countVoltage-1] = np.mean(j_p[:N-1])
                j_p_std[countVoltage-1] = np.std(j_p[:N-1])                           
                print("j_p_mean:",np.double(j_cop*j_p_mean[countVoltage-1]))
                print("j_p_std:", j_p_std[countVoltage -1])
    #             print(j_p*j_cop)
                print("Error in phi_p",errorPlot_p[i])
                print("Error in psi", errorPlot[i])
                
                const1 = np.double(j_cop*j_p_mean[countVoltage-1])
                print(const1)
                plt.title(r" j_p",fontsize=16)
                plt.plot((j_cop*((j_p[:N-1]))),"-g", label='Hole Current')
                plt.ylim(const1 - const1*(10**-1), const1 + const1*(10**-1))

                plt.ylabel("j")
                plt.xlabel("Node number")
                plt.show()
                print(j_p)
                print(np.min((phi_p - psi - np.roll(phi_p, 1) +np.roll(psi,1))))
                const2 = np.double(j_con*j_n_mean[countVoltage-1])
                
                plt.title(r" j_n",fontsize=16)
                plt.plot((j_con*((j_n[:N-1]))),"-b", label='Electron Current')
                plt.ylim(const2 - const2*(10**-1), const2 + const2*(10**-1))

                plt.ylabel("j")
                plt.xlabel("Node number")
                plt.show()

                print(j_n)
                
                plt.plot(errorPlot,"-r")
                plt.plot(errorPlot_p,"-b")
                plt.plot(errorPlot_n,"-g")

                plt.ylabel("Error")
                plt.xlabel("tries")
                plt.show()

                plt.title(r" Error in Electron potential" r" $ \Phi_n $",fontsize=16)
                plt.plot(errorPlot_n,"-g")
                plt.ylabel("Error")
                plt.xlabel("tries")
                plt.show()

                plt.title(r" Error in Hole potential" r" $ \Phi_p $",fontsize=16)
                plt.plot(errorPlot_p,"-b")
                plt.ylabel("Error")
                plt.xlabel("tries")
                plt.show()

                plt.title(r" Error in potential" r" $ \Psi $",fontsize=16)
                plt.plot(errorPlot,"-r")
                plt.ylabel("Error")
                plt.xlabel("tries")
                plt.show() 
                
                countVoltage += 1

                
#                 import pdb; pdb.set_trace()
            
    
    
            if(i>iterations-2):
                print("CONVERGENCE LIMIT NOT REACHED")
                print(errorPlot_p[i],errorPlot_n[i], errorPlot[i] )

        
            V += stepSizeVoltage
            
        end = time.time()
        print(end-start)
        print("Done")
        return run1
                
                
                                          





# +

# import numpy as np
# import material as mat
# import sample
# import samplePlusMaterial as spm
# from run import run
# from run import state
# from run_file import run_file

# from ipywidgets import interact, interactive, fixed, interact_manual, FloatSlider
# import ipywidgets as widgets

# import time
# import pickle 
        
# #Reads the simulation parameters from a "run-file" which is defined using "launcher.ipynb"
# with open("run-commands/run-file", 'rb') as f:
#     data = pickle.load(f)


# sampleTest = sample.Sample(data.N, data.L, data.T, data.C) #Set the sample parameters
# # materialTest = data.mat #Set the material
# materialTest = mat.GaN
# workBench = spm.SamplePlusMaterial(sampleTest, materialTest) #combine the sample and material

# workBench.calc_initial_guess() #Calculate initial guess via charge conservation

# #         self.solver = explicit_solver.Solver(self.workBench)
# solver = Solver(workBench)
# # solver = solver.Solver(workBench)
# # self.solver = arbitrary_precision.Solver(self.workBench)
# #         self.solver = hyperbolic_solver.Solver(self.workBench) #Call a specific solver class
# #         self.solver = solver.Solver(self.workBench)
# init = solver.calc_built_in_potential() #call the solver's built_in_potential class





# #         #Store the run returned in a file name specified by the run-file
# run_returned = solver.drift_diffusion_solver(init, data.initial_pot, data.numberOfDataPoints, data.convergence, data.maxIter, 1)
# # run_returned = self.solver.arbitrary_precision_drift_diffusion_solver(init, data.initial_pot, data.numberOfDataPoints, data.convergence, data.maxIter, 0)


# # test = interface()
# +
# with open(data.savePath, 'wb') as f:
#         # Pickle the 'data' dictionary using the highest protocol available.
#     pickle.dump(run_returned, f, pickle.HIGHEST_PROTOCOL)
# -




