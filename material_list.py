from sample import material

# +
name = "Si"
E_v = 0 ## Energy of the valence band in eV
E_c = 1.12 ## Energy of the conduction band in eV 
E_t = (E_c - 0.44)
N_v = 320 ## Density of states of the valence band in units of 10^17 cm^-3
N_c = 180 ## Density of states of the conduction band in units of 10^17 cm^-3
epsilon_s  = 11.7*8.854*10**-12 ## The permittivity of Si in SI units
mu_n = 1400 #The mobility of electrons in Si 
mu_p = 450 #The mobility of holes in Si 
r_sp = 10**-14 #The spontaneous recombination rate in Si
tau_p = 10**-7 #The characteristic time constants in SRH recombination
tau_n = 10**-7 #The characteristic time constants in SRH recombination
C_n = 0.11 ## Auger coefficient in units of 10^-29 cm^6/s
C_p = 0.03 ## Auger coefficient in units of 10^-29 cm^6/s

silicon = material(name, E_v, E_c, E_t, N_v, N_c, epsilon_s, mu_n, mu_p, tau_p, tau_n, C_n, C_p, r_sp)

# +
name = "GaN"
E_v = 0 ## Energy of the valence band in eV
E_c = 3.2 ## Energy of the conduction band in eV 
E_t = (E_c - 0.44)

N_v = 462 ## Density of states of the valence band in units of 10^17 cm^-3
# N_v = 22 ## Density of states of the valence band in units of 10^17 cm^-3

N_c = 22 ## Density of states of the conduction band in units of 10^17 cm^-3
epsilon_s  = 8.9*8.854*10**-12 ## The permittivity of Si in SI units
mu_n = 440  #The mobility of electrons in GaN 
mu_p = 120 #The mobility of holes in GaN 
r_sp = 10**-14 #The spontaneous recombination rate in Si
tau_p = 10**-7 #The characteristic time constants in SRH recombination
tau_n = 10**-7 #The characteristic time constants in SRH recombination
C_n = 0.11 ## Auger coefficient in units of 10^-29 cm^6/s
C_p = 0.03 ## Auger coefficient in units of 10^-29 cm^6/s

GaN = material(name, E_v, E_c, E_t, N_v, N_c, epsilon_s, mu_n, mu_p, tau_p, tau_n, C_n, C_p, r_sp)
# -


