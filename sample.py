class Sample:
    def __init__(self, N, L, T, C):
        self.N = N ## The number of discrete steps in the simulation
        self.L = L ## The length of the sample in microns
        self.T = T ## The temperature in K
        self.U_T = (1.38064852*10**-23*self.T)/(1.6*10**-19)
        self.kT = self.U_T ## kT in eV
        self.hSize = (self.L*10**-6)/(self.N-1)
        self.C = C       


class material:
    def __init__(self, name, E_v, E_c, E_t, N_v, N_c, epsilon_s, mu_n, mu_p, tau_p, tau_n, C_n, C_p, r_sp ):
        self.name = name
        self.E_v = E_v ## Energy of the valence band in eV
        self.E_c = E_c ## Energy of the conduction band in eV 
        self.E_t = E_t
        self.N_v = N_v ## Density of states of the valence band in units of 10^17 cm^-3
        self.N_c = N_c ## Density of states of the conduction band in units of 10^17 cm^-3
        self.epsilon_s  = epsilon_s ## The permittivity in SI units        
        self.mu_n = mu_n #The mobility of electrons in cm^2/Vs
        self.mu_p = mu_p #The mobility of holes in cm^2/Vs
        self.tau_p = tau_p #The characteristic time constants in SRH recombination 
        self.tau_n = tau_n #The characteristic time constants in SRH recombination
        self.C_n = C_n ## Auger coefficient in units of 10^-29 cm^6/s
        self.C_p = C_p ## Auger coefficient in units of 10^-29 cm^6/s
        self.r_sp = r_sp


# +
import numpy as np
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt


class SamplePlusMaterial:
    def __init__(self, sample, material):
        self.sample = sample
        self.material = material
        
        ##Calculated below
        self.r_sp_n = 0
        self.r_sp_p = 0
        self.r_norm_n = 0
        self.r_norm_p = 0
        self.r_srh_con = 0
        self.r_srh_cop = 0
        self.p_t = 0
        self.n_t = 0
        self.r_aug_n = 0
        self.r_aug_p = 0
        self.j_cop = 0
        self.j_con = 0
        self.N_I = np.ones(self.sample.N)
        self.psi_0 = np.zeros(self.sample.N)
        self.qEpsilon = 0

        self.calc_dependant_quantities()
        
        
    
    def calc_dependant_quantities(self):
        N = self.sample.N
        U_T = self.sample.U_T
        kT = self.sample.kT
        hSize = self.sample.hSize
        mu_n = self.material.mu_n
        mu_p = self.material.mu_p
        N_v = self.material.N_v
        N_c = self.material.N_c
        E_t = self.material.E_t
        E_c = self.material.E_c
        E_v = self.material.E_v
        C_n = self.material.C_n
        C_p = self.material.C_p
        r_sp = self.material.r_sp
        epsilon_s = self.material.epsilon_s
        hSize = self.sample.hSize
        
        self.r_sp_n = hSize**2*(r_sp)*10**21/(mu_n*U_T)
        self.r_sp_p = hSize**2*(r_sp)*10**21/(mu_p*U_T)
        self.r_norm_n = hSize**2/(mu_n*U_T)
        self.r_norm_p = hSize**2/(mu_p*U_T)
        self.r_srh_con = hSize**2*10**4/(mu_n*U_T)
        self.r_srh_cop = hSize**2*10**4/(mu_p*U_T)
        self.p_t = N_v*np.exp(-(E_t-E_v)/kT)
        self.n_t = N_c*np.exp(-(E_c-E_t)/kT)
        self.r_aug_n = hSize**2*C_n*10**9/(mu_n*U_T)
        self.r_aug_p = hSize**2*C_p*10**9/(mu_p*U_T)
        self.j_cop = 1.6*mu_p*U_T/hSize
        self.j_con = 1.6*mu_n*U_T/hSize
        self.N_I = np.ones(N)*N_c*N_v*np.exp(-(E_c-E_v)/kT) ## The intrinsic charge carrier concentration squared
        self.N_I = self.N_I**0.5 ## The intrinsic charge concentration
        self.qEpsilon = (1.6*10**-19)/epsilon_s ## q/Epsilon
        self.qEpsilon = self.qEpsilon*hSize**2*10**23 ## This is (q/Epsilon)*h^2. Used as a normalization \


    
    def calc_initial_guess(self):
        
        
        N = self.sample.N
        C = self.sample.C
        U_T = self.sample.U_T
        E_c = self.material.E_c
        E_v = self.material.E_v
        N_I = self.N_I
        N_c = self.material.N_c
        N_v = self.material.N_v
        
        
        psi_0 = np.zeros(N) ## The initial guess for the potential that satisfies local charge neutrality
        
        ##This loop handles the case where a negative and large C can cause the log to diverge
        for i in range(N):
            if(C[i] < 0 and abs(C[i]/N_I[i]) > 10**7):
                psi_0[i] = (E_c-E_v)/(2)  - 0.5*(U_T)*np.log(N_c/N_v) + U_T*np.log(-N_I[i]/C[i] + N_I[i]**3/C[i]**3)
            else:
                psi_0[i] = (E_c-E_v)/(2)  - 0.5*(U_T)*np.log(N_c/N_v) + U_T*np.log((C[i] + (C[i]**2 + 4*N_I[i]**2)**0.5)/(2*N_I[i]))
                
                
        self.psi_0 = psi_0
# -



