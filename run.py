# +
import numpy as np

from sample import SamplePlusMaterial as spm


# #These are classes that I have defined to define data structures that can be used to store the results
# # of the simulations in a file. In addition, the reduce the no. of lines of code.


# #The state class defines a class to store the state of the simulation at a given time
class state:
        
    def __init__(self, psi, phi_p, phi_n, p, n, j_p, j_n, V):
        self.psi = psi ## The potential as function of x
        self.phi_p = phi_p
        self.phi_n = phi_n
        self.p = p
        self.n = n
        self.bias = V
        self.j_n = j_n
        self.j_p = j_p
        

#The run class defines a simulation run and stores the states at all the simulated biases. 
class run:
    def __init__(self, numberOfDataPoints, workbench):
        self.numberOfDataPoints = numberOfDataPoints
        self.bias_V = np.zeros(self.numberOfDataPoints)
        self.bias_j = np.zeros(self.numberOfDataPoints)
        self.states = []
        self.count = 0
        self.j_p_mean = np.zeros(self.numberOfDataPoints)
        self.j_p_std = np.zeros(self.numberOfDataPoints)
        self.j_n_mean = np.zeros(self.numberOfDataPoints)
        self.j_n_std = np.zeros(self.numberOfDataPoints)
        self.workbench = workbench
        
    def add(self, temp_state, N):
        self.states.append(temp_state)
        self.bias_V[self.count] = temp_state.bias
        self.j_p_mean[self.count] = np.mean(np.abs(temp_state.j_p[:N-1]))
        self.j_p_std[self.count] = np.std((temp_state.j_p[:N-1]))
        self.j_n_mean[self.count] = np.mean(np.abs(temp_state.j_n[:N-1]))
        self.j_n_std[self.count] = np.std((temp_state.j_n[:N-1]))
        self.bias_j[self.count] = np.mean(self.workbench.j_con*temp_state.j_n[:N-1] + self.workbench.j_cop*temp_state.j_p[:N-1])        
        self.count = self.count + 1
        
    def reset_counter(self):
        self.count = 0
        
    def merge(self, run2):
        if(self.numberOfDataPoints == 0):
            self.numberOfDataPoints = run2.numberOfDataPoints
            self.bias_V = run2.bias_V
            self.bias_j = run2.bias_j
            self.states = run2.states
            print("self.states",len(self.states))
            self.count = run2.count
            self.j_p_mean = run2.j_p_mean
            self.j_p_std = run2.j_p_std
            self.j_n_mean = run2.j_n_mean
            self.j_n_std = run2.j_n_std
            
        else:
            self.numberOfDataPoints = self.numberOfDataPoints + run2.numberOfDataPoints
            self.bias_V = np.concatenate([self.bias_V,run2.bias_V])
            self.bias_j = np.concatenate([self.bias_j,run2.bias_j])
            self.states.extend(run2.states)
            
            print("self.states",len(self.states))
            print(len(run2.states))
            self.count = self.count + run2.count
            self.j_p_mean = np.concatenate([self.j_p_mean,run2.j_p_mean])
            self.j_p_std = np.concatenate([self.j_p_std,run2.j_p_std])
            self.j_n_mean = np.concatenate([self.j_n_mean,run2.j_n_mean])
            self.j_n_std = np.concatenate([self.j_n_std,run2.j_n_std])
    
    def plot_something(self):
        j = np.zeros(len(self.states))
        i = 0
        N = self.states[0].j_p.size
        for sta in self.states:
            j[i] = -np.mean((self.workbench.j_con*sta.j_n[:N-1]) + self.workbench.j_cop*sta.j_p[:N-1])
            i+=1
        
        import matplotlib.pyplot as plt
        plt.plot(j)
        plt.show()


# -


