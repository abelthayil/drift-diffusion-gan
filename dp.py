# +
from __future__ import print_function

import numpy as np
import sample
from sample import material as mat
import matplotlib.pyplot as plt
from sample import SamplePlusMaterial
from run import run
from run import state
import material_list

from ipywidgets import interact, interactive, fixed, interact_manual, FloatSlider
import ipywidgets as widgets

import time
import pickle





class display_launcher:
    
    def __init__(self, N, L, T, C, mat):
        self.N = N
        self.L = L
        self.T = T
        self.C = C
        self.mat = mat
        self.display_interface()
    
    
    def generate_run_file(self, initial_pot, final_pot, convergence, stepSize, maxIter, savePath):
        print("Initial Voltage", initial_pot)
        print("Final Voltage",final_pot)
        print("Convergence Limit", convergence)
        print("Step Size", stepSize)
        print("Max Number of Iterations", maxIter)
        print("Save Path", savePath)
        numberOfDataPoints = int((final_pot - initial_pot)/stepSize + 1) 
        print("Number of Data Points", numberOfDataPoints)

        
        launcher_run_file = run_file(self.N, self.L, self.T, self.C, self.mat, initial_pot, final_pot, numberOfDataPoints, convergence, stepSize, savePath, maxIter )
        launcher_run_file.generate_file()
        
    
    #Handles the display interface
    def display_interface(self):
        
        #Asks whether a new simulation is to be run?
        newSimulationOptions = ["Yes", "No, continue from a previous run."]
        style = {'description_width': 'initial'}
        newSimulationOptionsWidget = widgets.RadioButtons(
            options=newSimulationOptions,
            description='New Simulation?',
            style = style,
            layout=widgets.Layout(width="60%")
        )
        display(newSimulationOptionsWidget)
        
        
        #This button is triggered by a press and depending on whether a new simulation is to be run,
        #displays the relevant menu.
        button = widgets.Button(description="Next")
        output = widgets.Output()
        display(button, output)

        
        
        
        
        
        
        #Widget that displays the initial voltage when no previous file is selected,
        #corresponding to a simulation from scratch.
        initialVoltWidget2 = widgets.FloatText(
                        value=0.1,
                        description='Initial Voltage:',
                        style = style,
                        disabled=True
                    )
        
        #Widget that displays the final voltage when no previous file is selected,
        #corresponding to a simulation from scratch.
        finalVoltWidget2 = widgets.FloatText(
        value=5,
        description='Final Voltage:',
        style = style,
        disabled=False
        )
        
        #Actions when the new simulation button is clicked
        def on_button_clicked(b):
            
                   
            with output:
                
                
                #If you continue from a previous simulation            
                if(newSimulationOptionsWidget.index == 1): 
                    from IPython.display import clear_output
                    clear_output()
                    
                    
                    #Generates a list of files to read previous simulations from
                    from os import listdir
                    from os.path import isfile, join
                    mypath = "./results/" #This is where all the previous simulations are stored
                    files = [f for f in listdir(mypath) if isfile(join(mypath, f)) and f.endswith("pickle")]
                    files.sort()
                    fileDisplay = []    
                    for i in range(len(files)):
                        fileDisplay.append(str(i)+"    "+files[i])
                    #Widget that lists all the files in a select drop-down widget.
                    fileDisplayWidget = widgets.Select(
                        options=fileDisplay,
                        description='Select the file to read from:',
                        layout=widgets.Layout(width="60%"),
                        style = style
                    )

                    
                    display(fileDisplayWidget)

                    
                    #Select File Button
                    FileSelectButton = widgets.Button(description="Select File!")
                    FileSelectOutput = widgets.Output()
                    display(FileSelectButton, FileSelectOutput)
                    FileSelectButton.on_click(on_file_select_button_clicked)
                
                
                
                
                #Action when you continue from a previous simulation, and when you select the file.
                def on_file_select_button_clicked(b):
                    with FileSelectOutput:
                            from IPython.display import clear_output
                            clear_output()
                            #Based on the file selected, the following widget displays the initial voltage of the
                            #new run (the last voltage in the run of the selected file)
                            with open('./results/'+files[fileDisplayWidget.index], 'rb') as f:
                                                data = pickle.load(f)
                            initialVoltWidget = widgets.FloatText(
                                                    value=np.around(data.bias_V[-1], decimals=3),
                                                    description='Initial Voltage:',
                                                    style = style,
                                                    disabled=True
                                                )

                            #Widget that displays the final voltage of the new simulation    
                            finalVoltWidget = widgets.FloatText(
                                                value=initialVoltWidget.value + 5,
                                                description='Final Voltage:',
                                                style = style,
                                                disabled=False
                                                )

                            display(initialVoltWidget)
                            display(finalVoltWidget)
                            
                            

                            
                #If you start a new simulation    
                if(newSimulationOptionsWidget.index == 0):
                    from IPython.display import clear_output
                    clear_output()
                    display(initialVoltWidget2)
                    display(finalVoltWidget2)
                    
                    
                    
            
            
            
                    
      
        #When you click the new simulation (Next) button?
        button.on_click(on_button_clicked)
        
        #Text Box to specify the convergence limit
        convergenceLimitWidget = widgets.FloatText(
        value=-15,
        description='Convergence Limit in Log:',
        style = style,
        disabled=False
        )
        
        #Text Box to specify the step size
        stepSizeWidget = widgets.FloatText(
                value=0.1,
                description='Step Size Voltage:',
                style = style,
                disabled=False
                )
        
        #Text Box to specify the save Path
        savePathWidget = widgets.Text(
                    value='filename',
                    placeholder='Type something',
                    description='SavePath (name of the output file):',
                    disabled=False,
                    style = style
                )
        
        #Text Box to specify the max number of iterations
        MaxIterWidget = widgets.IntText(
                value=200000,
                description='Max. number of iterations:',
                style = style,
                disabled=False
                )
        
        #Button to launch the functions that generate the run file
        RunButton = widgets.Button(description="Generate Run File")
        RunButtonOutput = widgets.Output()

        
        #Another set of actions when the (Next) button is clicked
        def on_button_clicked_step(b):
            with output:
                display(stepSizeWidget) #Show step size widget
                display(convergenceLimitWidget) #Show convergence limit widget
                display(MaxIterWidget) #Show convergence limit widget
                display(savePathWidget) #Show save Path widget
                display(RunButton, RunButtonOutput) #show the generate run file button
                RunButton.on_click(on_run_button_clicked)
                
                
                
        #Calls the set of actions when the (Next) button is clicked
        button.on_click(on_button_clicked_step)
        
        
        #Defines the set of actions when the generate run file button is clicked
        def on_run_button_clicked(b):
            with RunButtonOutput:
                convergence = convergenceLimitWidget.value #Set the convergence limit value
                stepSize = stepSizeWidget.value #Set the step size value
                timestr = time.strftime("%Y%m%d-%H%M")
                savePath = "./results/"+savePathWidget.value #Set the save path with time stamp
                maxIter = MaxIterWidget.value #Set the max number of iterations
                #Set the initial and final bias voltages
                if(newSimulationOptionsWidget.index == 1):
                    initial_pot = initialVoltWidget.value + stepSize
                    final_pot = finalVoltWidget.value
                else:
                    initial_pot = initialVoltWidget2.value
                    final_pot = finalVoltWidget2.value
        
                #Generate the run file
                self.generate_run_file(initial_pot, final_pot, convergence, stepSize, maxIter, savePath)

                
                
                
#The class run_file specifies the variables that are entered into a run-file, read by interface to start a simulation.
class run_file:
    def __init__(self, N, L, T, C, mat, initial_pot, final_pot, numberOfDataPoints, convergence, stepSize, savePath, maxIter):
        self.N = N
        self.L = L
        self.T = T
        self.C = C
        self.mat = mat
        self.initial_pot = initial_pot
        self.final_pot = final_pot
        self.convergence = convergence
        self.stepSize = stepSize
        self.savePath = savePath
        self.maxIter = maxIter
        self.numberOfDataPoints = numberOfDataPoints
    
    #generate_file dumps the above data into a file.
    def generate_file(self):
        import pickle
        with open("run-commands/run-file", 'wb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)
            
        print("Run File Generated!")
# -


